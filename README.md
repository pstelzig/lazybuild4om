# lazybuild4om -- Using docker as a lazy and unofficial build helper for OpenModelica under Ubuntu Linux
lazybuild4om is a little experimental and strictly unofficial helper tool to build the open source simulation software [OpenModelica](https://openmodelica.org/) from source for a specific (old) version. This is simply to have some older versions of OpenModelica available before you definitely transition to the newest release. 

The best and official way to get [OpenModelica](https://openmodelica.org/download/) is to use the official installers for Linux, Microsoft Windows and Mac provided by [OpenModelica](https://openmodelica.org/). If you want to build a specific version from source, there are detailed [instructions on how to build from source](https://github.com/OpenModelica/OpenModelica#compilation) basically for every release. This is where you should turn to to obtain OpenModelica! OpenModelica is great!
Check out [OpenModelica's license terms](https://openmodelica.org/useresresources/license) first and make sure you agree to them before using OpenModelica!

lazybuild4om on the other hand
* merely helps you build OpenModelica *from source for a specific (old) version*
* for Debian or Ubuntu operating systems
* *without having to install the build dependencies* on your host operating system
* by using a docker image as the build environment
* is currently just an experiment and has several limitations, but worked for me!

Prerequisites: 
* A 64 bit [Debian](https://www.debian.org/) or [Ubuntu](https://ubuntu.com/) operating system and 
* a running [Docker installation](https://docs.docker.com/engine/install/ubuntu/). 

Note that depending on your docker installation you might need `sudo` rights to execute `docker`. 


## Why this build script
As said above, sometimes it is handy to keep some older versions of OpenModelica available before you transition to the newest release. If you [installed OpenModelica on Linux the official way](https://openmodelica.org/download/download-linux) as a Debian package via `apt-get`, you can of course [downgrade to an older version](https://openmodelica.org/download/download-linux) as explained (see the section "Installing Older Releases"). This however is a little cumbersome and lets you have only one version installed. 

The [OpenModelica project's GitHub repo](https://github.com/OpenModelica/OpenModelica) comes with a completely automated compilation procedure, see the [OpenModelica build instructions](https://github.com/OpenModelica/OpenModelica/blob/master/README.md). However, building form source requires the installation of various dependencies, which might be time-consuming and have unwanted effects on the system where the compilation takes place. Instead, one could do this inside a docker image, which is exactly what this build script is doing. 

If you want to use OpenModelica *inside* a docker container, see the [OpenModelica docker images](https://hub.docker.com/r/openmodelica/openmodelica) provided by the OpenModelica project. 


## Build using this script
The present helpers here are meant for a Linux Debian or Ubuntu operating system. To build from source using this repository's build script just clone this repository (change `ombuild` to your needs, make sure it is an empty folder). 

```
$ mkdir ombuild && cd ombuild
$ git clone https://gitlab.com/pstelzig/lazybuild4om.git
```

To build e.g. OpenModelica with the tag v1.14.2, just run

```
$ python3 build.py v1.14.2
```

(replace with the version tag you need). It should definitely work for versions 1.13 and 1.14 and their respective minor releases. This will create a folder `build_v1.14.2` in the `ombuild` directory (or whatever you chose) containing all the binaries, libs and includes that the OpenModelica build system would create on a `make install` call. You can then run e.g. OMEdit from `ombuild` by executing `./openmodelica/build_v1.14.2/bin/OMEdit`. 


## Known issues
* Does not work for latest release 1.16
* Better to use docker API for Python instead of Python's `os.system(...)` on the docker executable.
* If the host operating system uses different versions of dependencies than the base operating system in the docker image used for the build, the build artifacts might not run on the host


# License
lazybuild4om is open source software. lazybuild4om is provided "as is", without warranty of any kind. Usage is completely at your own risk. See the file `LICENSE`. 


# How to contribute
If you want to help me extend lazybuild4om, please drop me a message! Contributions are welcome!


# Author of lazybuild4om
Dr. Philipp Emanuel Stelzig, Copyright (c), 2020. 

