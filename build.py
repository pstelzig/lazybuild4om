"""
Simple helper project to build OpenModelica by version on a Debian or Ubuntu host system using docker 
as a build environment and copy the build artifacts to the host system.

Copyright (c) Dr. Philipp Emanuel Stelzig, 2020.

MIT License. See the project's LICENSE file.

"""

import argparse
import sys
import os
import pathlib
from packaging import version


def build_openmodelica(version_tag):
    # Determine version of OpenModelica to be built. 
    omversion = version.parse(version_tag)
    
    # Check if old build folder exists
    build_path = pathlib.Path(__file__).parent.absolute() / "build_{}".format(version_tag)
    if build_path.exists():
        print("[ERROR] Error building required OpenModelica version {} because the build target folder \n\t{}\nalready exists. "
              "Remove or rename it before you can build with this script. Aborting. ".format(version_tag, build_path))
        sys.exit(-1)
                  
    os.mkdir(build_path)    
    
    # If desired OpenModelica version >= v1.14.2 use Ubuntu 20.04 as base image, otherwise 18.04
    ubuntu_version = "20.04"
    if omversion < version.Version("1.14.2"):
        ubuntu_version = "18.04"
        
    os.system("docker build -t openmodelica_build_{0} "
          "--build-arg OMVERSIONCHECKOUT={0} --build-arg UBUNTU_VERSION={1} ./".format(version_tag, ubuntu_version))
    os.system("docker container create --name ombuildcontainerinstance_{0} openmodelica_build_{0}".format(version_tag))
    
    os.system("docker cp ombuildcontainerinstance_{0}:/opt/openmodelica build_{0}/".format(version_tag))
        
    return
    
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("tag", 
                        help="Tag of the OpenModelica version to be build. Must be same string as tag in git repo", 
                        type=str)
                        
    args = parser.parse_args()
    
    build_openmodelica(args.tag)

