# Simple helper project to build OpenModelica by version on a Debian or Ubuntu host system using docker 
# as a build environment and copy the build artifacts to the host system.
# 
# Copyright (c) Dr. Philipp Emanuel Stelzig, 2020.
# 
# MIT License. See the project's LICENSE file.

ARG UBUNTU_VERSION

FROM ubuntu:${UBUNTU_VERSION}

ARG OMVERSIONCHECKOUT
ARG DEBIAN_FRONTEND=noninteractive

# Update packages
RUN apt-get update --yes
RUN apt-get upgrade --yes

# Install OpenModelica dependencies
RUN apt-get install --yes autoconf automake libtool g++ gfortran git clang cmake libboost-all-dev \
                          hwloc libblas3 libblas-dev liblapack3 liblapack-dev liblpsolve55-dev libsundials-dev \
                          libhdf5-dev libexpat1-dev libncurses5 omniorb openscenegraph \
                          qt5-default qt5-qmake libqt5opengl5-dev libqt5webkit5-dev libqt5xmlpatterns5-dev libqt5svg5-dev qttools5-dev-tools \
                          curl libcurl4-gnutls-dev libreadline-dev gettext  libncurses5-dev libncurses5 flex default-jre

# Compile OpenModelica like in https://github.com/OpenModelica/OpenModelica/blob/master/OMCompiler/README.Linux.md
RUN git clone https://openmodelica.org/git-readonly/OpenModelica.git OpenModelica && cd OpenModelica
RUN cd OpenModelica && git checkout $OMVERSIONCHECKOUT 
RUN cd OpenModelica && git submodule update --init --recursive && rm -rf OMOptim

RUN mkdir -p /opt/openmodelica

RUN cd OpenModelica && autoconf && ./configure --with-omlibrary=core --prefix=/opt/openmodelica --with-cppruntime CC=clang CXX=clang++
RUN cd OpenModelica && make -j$(nproc) 
RUN cd OpenModelica && make install



